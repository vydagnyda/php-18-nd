<?php

require_once 'ProductInterface.php';
class Cart
{
    protected $products = [];
    protected $totalPrice = 0;

    public function addProduct(ProductInterface $product)
    {
        $this->products[] = $product;
        $this->totalPrice += $product->getPrice(); 
    }

    /**
     * Get the value of totalPrice
     */ 
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    public function checkComputer(ProductInterface $products):bool
    {
        $memoryCount = 0;
        $cardCount = 0;
        $processorCount = 0;
        foreach ($products as $product) 
        {
            switch ($product->getName()) {
                case 'Memory':
                    $memoryCount++;
                    break;
                case 'Processor card':
                    $processorCount++;
                    break;
                case 'Card':
                    $cardCount++;
                    break;
                case 'SSD Card':
                    $cardCount++;
                    break;
            }           
        }
        if (($processorCount > 0) && ($memoryCount > 0) && ($cardCount > 0)) {
            $result = true;
        } else { 
            $result = false; 
        }
        return $result;
    }

}