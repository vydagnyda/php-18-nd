<?php
require_once 'Device.php';

final class Processor extends Device 
{
    protected $socket;
    protected $speed;
    protected $cores;

    public function setSocket($socket)
    {
        $this->socket = $socket;

        return $this;
    }

    public function setSpeed($speed)
    {
        $this->speed = $speed;

        return $this;
    }

    public function setCores($cores)
    {
        $this->cores = $cores;

        return $this;
    }

    public function getName()
    {
        return 'Processor card';
    }

    public function __toString()
    {
        return 'Processor drive';
    }
}