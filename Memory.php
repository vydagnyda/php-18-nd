<?php
require_once 'Device.php';
require_once 'ProductInterface.php';

final class Memory extends Device implements ProductInterface
{
    // protected $type;
    // protected $amount;
    // protected $speed;

    protected $price;
   
    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    public function getName(): string
    {
        return 'Memory';
    }

    // public function setType($type)
    // {
    //     $this->type = $type;

    //     return $this;
    // }
    // public function setAmount($amount)
    // {
    //     $this->amount = $amount;

    //     return $this;
    // }

    // public function setSpeed($speed)
    // {
    //     $this->speed = $speed;

    //     return $this;
    // }

    // public function getCard()
    // {
    //     return 'Memory card';
    // }

    // public function __toString()
    // {
    //     return 'Memory drive';
    // }
}
