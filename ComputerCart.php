<?php
require_once 'ProductInterface.php';

class ComputerCart 
{
    protected $products = [];
    protected $totalPrice = 0;

    public function addProduct(ProductInterface $product)
    {
        $this->products[] = $product;
        $this->totalPrice += $product->getPrice();
    }

    /**
     * Get the value of totalPrice
     */ 
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }
}