<?php
require_once 'Device.php';

class HardDrive extends Device 
{
    protected $amount;

    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    public function getName()
    {
        return 'Card';
    }

    public function __toString()
    {
        return 'Hard drive';
    }
}